##
## EPITECH PROJECT, 2019
## Makefile
## File description:
## My makefile for the second rush
##

SRC =	./src/main.c \
		./src/game.c \
		./src/map.c \
		./src/get_info.c \
		./src/navigate.c \
		./src/sokowin.c

OBJ =	$(SRC:.c=.o)

NAME =	my_sokoban
CFLAGS = -Wall -Wextra -I./include/ -g

all :	$(NAME)

$(NAME) :	$(OBJ)
	make -C lib/my
	gcc -o $(NAME) $(OBJ) -L./lib/my -lmy -lncurses

clean :
	rm -f $(OBJ)
	make clean -C lib/my
	rm -f ~* \#*\#

fclean :	clean
	rm -f $(NAME)
	make fclean -C lib/my

re :	fclean all

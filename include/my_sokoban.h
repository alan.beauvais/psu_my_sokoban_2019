/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** my_sokoban
*/

#ifndef MY_SOKOBAN_H_
#define MY_SOKOBAN_H_

#include "struct.h"

// game
int create_game(game_t *game, char *filepath);
int render_game(game_t *game);
void finish_game(game_t *game);

// map
char *get_map(char *filepath);
char **create_array(char *map, int line_count, int line_size);
void fill_array(char **array, char *map);
void print_map(map_t map);
void clear_copy(game_t *game);

// getinfo
pos_t get_p_position(map_t map);
int get_nb_box(char *map);

// navigate
void navigate(int key, pos_t *player, game_t *game);
void my_move(game_t *game, pos_t *player, pos_t movement);
int move_boxes(game_t *game, pos_t move, pos_t np_pl, pos_t *pl);

// sokowin
int get_nb_obj(map_t map);

#endif /* !MY_SOKOBAN_H_ */

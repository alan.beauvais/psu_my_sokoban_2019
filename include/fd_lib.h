/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** fd_lib
*/

#ifndef FD_LIB_H_
#define FD_LIB_H_

int fd_open(char const *filepath);
int get_file_size(int fd);
char *read_file(int fd, int size);
int get_line_size(char const *str);
int count_line(char const *buffer);

#endif /* !FD_LIB_H_ */

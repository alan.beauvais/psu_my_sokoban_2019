/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** my
*/

#ifndef MY_H_
#define MY_H_

int my_putstr(char const *str);
int my_putchar(char c);
int my_strlen(char const *str);

#endif /* !MY_H_ */

/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** struct
*/

#ifndef STRUCT_H_
#define STRUCT_H_

typedef struct map
{
    char **array;
    int line_c;
    int line_s;
} map_t;

typedef struct position
{
    int x;
    int y;
} pos_t;

typedef struct game
{
    map_t map;
    map_t copy;
    pos_t player_pos;
    int box_nb;
    int isStart;
    int moves;
} game_t;

#endif /* !STRUCT_H_ */

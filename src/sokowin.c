/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** sokowin
*/

#include "my_sokoban.h"
#include "struct.h"

int get_nb_obj(map_t map)
{
    int count = 0;

    for (int x = 0; x < map.line_c; x++) {
        for (int y = 0; y < map.line_s; y++) {
            count += map.array[x][y] == 'O' ? 1 : 0;
        }
    }
    return count;
}
/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** main
*/

#include <stdlib.h>
#include <unistd.h>

#include "my.h"
#include "my_sokoban.h"
#include "fd_lib.h"
#include "struct.h"

int print_info(void)
{
    my_putstr("USAGE\n");
    my_putstr("\t./my_sokoban map\n");
    my_putstr("DESCRIPTION\n");
    my_putstr("\tmap\tfile representing the warehouse map, containing :\n");
    my_putstr("\t\t‘#’ for walls, ‘P’ for the player,\n");
    my_putstr("\t\t‘X’ for boxes and ‘O’ for storage locations.\n");
    return EXIT_SUCCESS;
}

int main(int ac, char **av)
{
    game_t game;
    int fd = fd_open(av[1]);
    char buffer[10];
    int read_file = read(fd, buffer, 10);
    int g_state = 0;

    if (ac != 2)
        return EXIT_FAILURE;
    if (av[1][0] == '-' && av[1][1] == 'h')
        return print_info();
    if (read_file < 1)
        return EXIT_FAILURE;
    while (g_state == 0) {
        g_state = create_game(&game, av[1]);
        if (g_state == 2)
            return EXIT_FAILURE;
        g_state = render_game(&game);
    }
    return EXIT_SUCCESS;
}
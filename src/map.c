/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** str_to_array
*/

#include <stdlib.h>
#include <ncurses.h>

// My include
#include "fd_lib.h"
#include "my_sokoban.h"
#include "struct.h"
#include "my.h"

char *get_map(char *filepath)
{
    int fd = fd_open(filepath);
    int file_size = get_file_size(fd);

    return read_file(fd, file_size);
}

char **create_array(char *map, int line_count, int line_size)
{
    char **map_array = malloc(sizeof(char*) * (line_count + 2));
    int i = 0;

    for (; i <= line_count; i++)
        map_array[i] = malloc(sizeof(char) * line_size);
    map_array[i] = NULL;
    fill_array(map_array, map);
    return map_array;
}

void fill_array(char **array, char *map)
{
    int x = 0;
    int y = 0;
    int i = 0;

    while (map[i] != '\0') {
        if (map[i] == '\n') {
            map[i] = '\0';
            x++;
            y = 0;
        }
        else {
            array[x][y] = map[i];
            y++;
        }
        i++;
    }
}

void print_map(map_t map)
{
    for (int i = 0; i <= map.line_c; i++) {
        for (int j = 0; j < map.line_s; j++)
            printw("%c", map.array[i][j]);
        printw("\n");
    }
}

void clear_copy(game_t *game)
{
    char **map = game->copy.array;
    for (int i = 0; i < game->copy.line_c; i++) {
        for (int j = 0; j < game->copy.line_s; j++)
            map[i][j] = map[i][j] == 'X' ? ' ' : map[i][j];
    }
    map[game->player_pos.x][game->player_pos.y] = ' ';
    game->copy.array = map;
}
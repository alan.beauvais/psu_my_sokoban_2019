/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** my_sokoban
*/

#include <ncurses.h>

#include <stdlib.h>
#include "fd_lib.h"
#include "my_sokoban.h"
#include "my.h"
#include "struct.h"

// init my map
map_t init_map(char *map)
{
    map_t map_s;

    map_s.line_c = count_line(map);
    map_s.line_s = get_line_size(map);
    map_s.array = create_array(map, map_s.line_c, map_s.line_s);
    return map_s;
}

// Create my game and set all my variable
int create_game(game_t *game, char *filepath)
{
    char *map = get_map(filepath);

    game->map = init_map(map);
    game->copy = init_map(get_map(filepath));
    game->player_pos = get_p_position(game->map);
    game->box_nb = get_nb_box(map);
    game->isStart = 1;
    game->moves = 0;
    clear_copy(game);
    return 0;
}

// Render the game in my term
int render_game(game_t *game)
{
    int ch;

    initscr();
    raw();
    keypad(stdscr, TRUE);
    noecho();
    while (game->isStart) {
        print_map(game->map);
        ch = getch();
        clear();
        if (ch == KEY_BACKSPACE)
            game->isStart = 0;
        if (ch == ' ')
            return 0;
        navigate(ch, &game->player_pos, game);
        finish_game(game);
        refresh();
    }
    endwin();
    return 1;
}

void finish_game(game_t *game)
{
    char *msg = "Congratulation, You win";
    int size = my_strlen(msg);

    if (get_nb_obj(game->map) == 0) {
        clear();
        mvprintw(LINES/2, (COLS / 2) - (size / 2), msg);
        getch();
        game->isStart = 0;
    }
}
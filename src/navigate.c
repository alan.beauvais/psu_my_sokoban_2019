/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** navigate
*/

#include <ncurses.h>
#include "my_sokoban.h"
#include "struct.h"

const pos_t UP = {-1, 0};
const pos_t DOWN = {1, 0};
const pos_t LEFT = {0, -1};
const pos_t RIGHT = {0, 1};

void navigate(int key, pos_t *player, game_t *game)
{
    switch (key) {
    case KEY_UP:
        my_move(game, player, UP);
        break;
    case KEY_DOWN:
        my_move(game, player, DOWN);
        break;
    case KEY_LEFT:
        my_move(game, player, LEFT);
        break;
    case KEY_RIGHT:
        my_move(game, player, RIGHT);
        break;
    }
}

void my_move(game_t *game, pos_t *pl, pos_t move)
{
    pos_t n_position;
    char c_npos;

    n_position.x = pl->x + move.x;
    n_position.y = pl->y + move.y;
    c_npos = game->map.array[n_position.x][n_position.y];
    if (c_npos != '#') {
        if (game->map.array[n_position.x][n_position.y] == 'X')
            move_boxes(game, move, n_position, pl);
        else {
            game->map.array[n_position.x][n_position.y] = 'P';
            game->map.array[pl->x][pl->y] = game->copy.array[pl->x][pl->y];
            *pl = n_position;
        }
        game->moves++;
    }
}

int move_boxes(game_t *game, pos_t move, pos_t np_pl, pos_t *pl)
{
    pos_t box_npos;
    char c_npos;

    box_npos.x = np_pl.x + move.x;
    box_npos.y = np_pl.y + move.y;
    c_npos = game->map.array[box_npos.x][box_npos.y];
    if (c_npos != '#' && c_npos != 'X') {
        game->map.array[box_npos.x][box_npos.y] = 'X';
        game->map.array[np_pl.x][np_pl.y] = 'P';
        game->map.array[pl->x][pl->y] = game->copy.array[pl->x][pl->y];
        *pl = np_pl;
        return 1;
    }
    else
        return 0;
}
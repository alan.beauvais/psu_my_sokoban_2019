/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** get_info
*/

#include <stdlib.h>

#include "my.h"
#include "my_sokoban.h"

pos_t get_p_position(map_t map)
{
    int x = 0;
    int y = 0;
    pos_t *p_position = malloc(sizeof(pos_t));


    while (x < map.line_s && map.array[x][y] != 'P') {
        if (map.array[x][y] != '\0')
            y++;
        else {
            y = 0;
            x++;
        }
    }
    p_position->x = x;
    p_position->y = y;
    return *p_position;
}

int get_nb_box(char *map)
{
    int count = 0;

    for (int i = 0; map[i] != '\0'; i++) {
        if (map[i] == 'X')
            count++;
    }
    return count;
}
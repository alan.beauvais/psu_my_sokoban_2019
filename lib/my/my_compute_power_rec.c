/*
** EPITECH PROJECT, 2019
** my_compute_power_rec.c
** File description:
** Task04 of the day05
*/

int my_compute_power_rec(int nb, int p)
{
    int result;

    if (p < 0)
        return 0;
    if (p == 0)
        return 1;
    else {
        if (p >= 1)
            result = my_compute_power_rec(nb, p - 1);
        result *= nb;
        return result;
    }
    return 0;
}

/*
** EPITECH PROJECT, 2019
** my_put_float.c
** File description:
** Display float number
*/

#include "../../include/my.h"

void my_put_double(double nb)
{
    int int_part = nb;
    double buffer = (nb - int_part) * 100;
    int dot_part = buffer;

    my_put_nbr(int_part);
    my_putchar('.');
    my_put_nbr(dot_part);
}

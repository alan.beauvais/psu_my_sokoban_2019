/*
** EPITECH PROJECT, 2019
** my_put_nbr.c
** File description:
** Put nbr
*/

void my_putchar(char c);

int my_put_nbr(int nb)
{
    int a = 1;

    if (nb == 0)
        my_putchar('0');
    if (nb < 0) {
        my_putchar('-');
        nb = -nb;
    }
    while (nb / a > 0)
        a *= 10;
    a /= 10;
    while (a > 0) {
        my_putchar(nb / a % 10 + 48);
        a /= 10;
    }
    return 0;
}

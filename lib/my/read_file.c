/*
** EPITECH PROJECT, 2019
** MY_FDLIB
** File description:
** read_file
*/

#include <stdlib.h>
#include <unistd.h>

char *read_file(int fd, int size)
{
    char *buffer = malloc(sizeof(char) * (size + 1));
    int read_file = read(fd, buffer, size);

    if (read_file == -1) {
        return NULL;
    }
    buffer[size] = '\0';
    return buffer;
}
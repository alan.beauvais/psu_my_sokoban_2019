/*
** EPITECH PROJECT, 2019
** PSU_my_sokoban_2019
** File description:
** count_line
*/

int count_line(char const *buffer)
{
    int count = 0;

    for (int i = 0; buffer[i] != '\0'; i++) {
        if (buffer[i] == '\n')
            count++;
    }
    return count;
}
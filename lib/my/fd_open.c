/*
** EPITECH PROJECT, 2019
** MY_FDLIB
** File description:
** fd_open
*/

#include <fcntl.h>

int fd_open(char const *filepath)
{
    int fd;

    fd = open(filepath, O_RDONLY);
    return fd;
}
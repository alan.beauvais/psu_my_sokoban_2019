/*
** EPITECH PROJECT, 2019
** MY_FDLIB
** File description:
** line_size
*/

int get_line_size(char const *str)
{
    int size = 0;
    int max_size = 0;

    for (int i = 0; str[i]; i++) {
        if (str[i] == '\n') {
            max_size = size > max_size ? size : max_size;
            size = 0;
        }
        size++;
    }
    return max_size;
}

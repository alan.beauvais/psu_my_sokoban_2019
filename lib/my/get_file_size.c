/*
** EPITECH PROJECT, 2019
** MY_FDLIB
** File description:
** get_file_size
*/

#include <sys/stat.h>
#include <sys/types.h>

int get_file_size(int fd)
{
    struct stat buf;

    fstat(fd, &buf);
    return buf.st_size;
}